from django.contrib import admin

from .models import Place, Booking, Payment, Rate, PlaceWork, TimeWork

class PlaceWorkInline(admin.TabularInline):
    model = PlaceWork
    extra = 3
class TimeWorkInline(admin.TabularInline):
    model = TimeWork
    extra = 3
class RateInline(admin.StackedInline):
    model = Rate
    extra = 2

class PlaceAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['title']}),
        ('Местоположение', {'fields': ['address','metro']}),
        ('Описание', {'fields': ['photo','description','price']}),
    ]
    inlines = [PlaceWorkInline,RateInline]
    list_display = ('title', 'metro')
    list_filter = ('metro', 'price')

class RateAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['title']}),
        ('Описание', {'fields': ['type','photo','description','capacity']}),
        ('Тарифы', {'fields': ['price_hour','price_week','price_month']}),
    ]
    list_display = ('title', 'place','photo')
    list_filter = ('place', 'type')

class BookingAdmin(admin.ModelAdmin):
    list_display = ('rate', 'start_time', 'time_over')
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('receipt_number', 'user', 'booking')
class TimeWorkAdmin(admin.ModelAdmin):
    list_display = ('week_day', 'start_time', 'end_time')
    

admin.site.register(Place, PlaceAdmin)
admin.site.register(Booking, BookingAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Rate, RateAdmin)
admin.site.register(TimeWork, TimeWorkAdmin)
# Register your models here.
