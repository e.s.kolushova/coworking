from django.shortcuts import render,get_object_or_404
from django.http import Http404,HttpResponse,HttpResponseRedirect
from .models import Place, Booking, Payment, Rate, PlaceWork, TimeWork
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
# Create your views here.


def index(request):
    return render(request,'index.html')

def place(request):
    places_list = Place.objects.order_by('title')
    # timework_list = PlaceWork.objects.prefetch_related(place = Place.id).all()
    placework_list = PlaceWork.objects.all
    timework_list = TimeWork.objects.all
    return render(request,'place/coworking.html', {'places_list':places_list,'placework_list':placework_list,'timework_list':timework_list})

def rate(request, place_id):
    try:
        a=Place.objects.get(id = place_id)
    except:
        raise Http404("Коворкинг не найден")
    rates_list = Rate.objects.order_by('type')
    return render(request,'rate/rate.html',{'place':a,'rates_list':rates_list})

def about(request):
    return render(request,'about_us/about_us.html')

def auth(request):
    return render(request,'login/auth.html')

def registration(request):
    return render(request,'login/registration.html')

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

# def index(request):
#     return HttpResponse("Hello, world. You're at the polls index.")
